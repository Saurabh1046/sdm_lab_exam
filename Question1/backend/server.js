const express = require('express')
const cors = require('cors')
const movieroute = require('./routes/movieroute')

const app = express();
app.use(express.json());
app.use(cors())

app.use('/movie', movieroute);

app.listen(4000, '0.0.0.0', () => {
    console.log("The App is listening on port number 4000");
})