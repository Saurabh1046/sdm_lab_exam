
const express = require('express')
const db = require('../db')
const utils = require('../utils')

const route = express.Router()




route.post('/', (request, response) => {

    console.log("IN post route")

    const { title, date, time, directorNamen } = request.body

    const query = `INSERT INTO MOVIE
                    ( movie_title , 
                     movie_release_date,
                     movie_time ,
                     director_name )
                    VALUES
                    ('${title}', '${date}', '${time}', '${directorNamen}')
                    `

    db.execute(query, (error, result) => {
        response.send(utils.createResult(error, result))
    })
})


//get by name
route.get('/:name', (request, response) => {

    const { name } = request.params

    const query = `SELECT * FROM movie WHERE movie_title = '${name}'`

    db.execute(query, (error, result) => {
        response.send(utils.createResult(error, result))
    })

})


//add movie

route.post('/', (request, response) => {

    const { title, date, time, directorNamen } = request.body

    const query = `INSERT INTO MOVIE
                    ( movie_title , 
                     movie_release_date,
                     movie_time ,
                     director_name )
                    VALUES
                    ('${title}', '${date}', '${time}', '${directorNamen}')
                    `

    db.execute(query, (error, result) => {
        response.send(utils.createResult(error, result))
    })
})


//update movie


route.put('/:id', (request, response) => {

    const { date, time } = request.body
    const { id } = request.params

    const query = `UPDATE MOVIE
                    SET
                     movie_release_date = '${date}',
                     movie_time= '${time}' ,
                    WHERE
                     movie_id = ${id}
                    `

    db.execute(query, (error, result) => {
        response.send(utils.createResult(error, result))
    })

})


//delete movie

route.delete('/:id', (request, response) => {


    const { id } = request.params

    const query = `DELETE FROM MOVIE
                    WHERE
                    movie_id = ${id}
                    `

    db.execute(query, (error, result) => {
        response.send(utils.createResult(error, result))
    })

})






//get all movies
route.get('/', (request, response) => {


    const query = `SELECT * FROM movie`

    db.execute(query, (error, result) => {
        response.send(utils.createResult(error, result))
    })

})







module.exports = route







module.exports = route
