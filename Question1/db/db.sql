create database sdm;
use sdm;


CREATE TABLE movie (
    movie_id INTEGER PRIMARY KEY AUTO_INCREMENT,
    movie_title varchar(50), 
    movie_release_date date,
    movie_time time,
    director_name varchar(50)
    );

